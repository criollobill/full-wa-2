class cProductList{
    constructor(Post){
        this.Post = Post;

        this.loaded = false;
        this.search = {texts: ''};
        this.products = [];

        this.init();
    }

    init(){
        this.Post('products/select-all').then(res => {
            this.products = res.map(product => {
                product.image = product.images ? product.images.split('!@!')[0] : '';
                return product;
            });
            this.loaded = true;
        }, err => {
            console.log('init', err);
        })
    }
}