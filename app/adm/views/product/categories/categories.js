class cProductCategories{
    constructor($rootScope, Post, $uibModal, $filter){
        this.rootScope = $rootScope;
        this.Post = Post;
        // this.ngToast = ngToast;
        this.uibModal = $uibModal;
        this.filter = $filter;

        this.loaded = false;
        this.links = [];
        this.levels = {
            data: {
                level1: [],
                level2: [],
                level3: [],
            },
            selected: {
                level1: 0,
                level2: 0,
            },
            new: {
                level1: '',
                level2: '',
                level3: '',
            },
        };

        this.rootScope.$on('REORDER_CATEGORIES', (event, level) => {
            const previousLevel = this.findLevel(level, 'previous');
            this.getLevels(this.levels.selected[previousLevel], level);
        });

        this.init();
    }

    init(){
        this.Post('categories/select-all').then(res => {
            this.links = res.map(category => category.link);
            this.getLevels(0, 'level1');
        }, err => {
           console.log('getAllCategories: ', err)
        });
    }

    getLevels(id = 0, level){
        this.Post('categories/select-by-id', {id_category: id}).then(res => {
            this.levels.data[level] = res;
            this.loaded = true;
        }, err => {
            console.log('getLevels: ', err);
            // this.ngToast.create({className: 'danger', content: 'Erro ao carregar categorias'});
        })
    }

    findLevel(currentLevel, action){
        if(action.match(/plus|mais|\+|next|proximo/)){
            return 'level' + (Number(currentLevel.replace('level', '')) + 1);
        }
        return 'level' + (Number(currentLevel.replace('level', '')) - 1);
    }

    setSelected(level, id){
        const nextLevel = this.findLevel(level, 'next');
        if(Number(this.levels.selected[level]) !== Number(id)){
            this.levels.selected[level] = id;

            // get data for selected id
            if(this.levels.data.hasOwnProperty(nextLevel)){
                this.levels.data[nextLevel] = [];
                this.levels.selected[nextLevel] = 0;

                // check if has one more level to clear
                const lastLevel = this.findLevel(nextLevel, 'next');
                if(this.levels.data.hasOwnProperty(lastLevel)){
                    this.levels.data[lastLevel] = [];
                    this.levels.selected[lastLevel] = 0;
                }

                this.getLevels(id, nextLevel);
            }
        }
    }

    create(currentLevel, description){
        if(description === ''){
            // this.ngToast.create({className: 'danger', content: '<i class="fa fa-close"></i> Preencha uma descrição'});
            return;
        }

        let link = this.filter('removeAccents')(description.replace(/ /gi, '-').toLowerCase());

        let sortSum = 0;
        if(currentLevel === 'level2') sortSum = 100;
        if(currentLevel === 'level3') sortSum = 200;


        const data = {id_category: 0, name: description, sort: this.levels.data[currentLevel].length + sortSum, link: link};
        let previousLevel = '';
        switch(currentLevel){
            case 'level2':
                previousLevel = this.findLevel(currentLevel, 'previous');
                if(this.levels.selected.hasOwnProperty(previousLevel)){
                    const levelId = this.levels.selected[previousLevel];
                    if(levelId){
                        // try find previous link and concat it to the current one
                        let previousLink = this.previousLink(currentLevel);
                        if(previousLink){
                            link = previousLink + '-' + link;
                        }

                        data.link = link;
                        data.id_category = levelId;
                    }else{
                        // this.ngToast.create({className: 'danger', content: '<i class="fa fa-close"></i> Selecione uma categoria Nível 1'});
                        return;
                    }
                }
                break;
            case 'level3':
                previousLevel = this.findLevel(currentLevel, 'previous');
                if(this.levels.selected.hasOwnProperty(previousLevel)){
                    const levelId = this.levels.selected[previousLevel];
                    if(levelId){
                        // try find previous link and concat it to the current one
                        let previousLink = this.previousLink(currentLevel);
                        if(previousLink){
                            link = previousLink + '-' + link;
                        }

                        data.link = link;
                        data.id_category = levelId;
                    }else{
                        // this.ngToast.create({className: 'danger', content: '<i class="fa fa-close"></i> Selecione uma categoria Nível 2'});
                        return;
                    }
                }
                break;
        }

        if(this.links.includes(data.link)){
            // this.ngToast.create({className: 'danger', content: 'Já existe uma categoria com este nome'});
            return;
        }

        this.Post('categories/create', data).then(() => {
            this.links.push(data.link);

            this.levels.new[currentLevel] = '';
            this.getLevels(this.levels.selected[previousLevel], currentLevel);
            // this.ngToast.create({content: '<i class="fa fa-check"></i> Categoria criada'});
        }, err => {
            console.log('create: ', err);
            // this.ngToast.create({className: 'danger', content: 'Erro ao tentar criar categoria'});
        })
    }

    edit(name, id, currentLevel){
        let link = this.filter('removeAccents')(name.replace(/ /gi, '-').toLowerCase());

        // try find previous link and concat it to the current one
        let previousLink = this.previousLink(currentLevel);
        if(previousLink){
            link = previousLink + '-' + link;
        }

        this.Post('categories/edit', {name: name, id: id, link: link}).then(() => {
            // this.ngToast.create({content: '<i class="fa fa-check"></i> Categoria editada'});
        }, err => {
            console.log('edit: ', err);
            // this.ngToast.create({className: 'danger', content: 'Erro ao tentar editar categoria'});
        })
    }

    delete(id, level, index){
        this.Post('categories/delete', {id: id}).then(() => {
            this.levels.data[level].splice(index, 1);
            // this.ngToast.create({content: '<i class="fa fa-check"></i> Categoria removida'});
        }, err => {
            console.log('delete: ', err);
            // this.ngToast.create({className: 'danger', content: 'Erro ao tentar remover categoria'});
        })
    }

    sort(level){
        if(!this.levels.data[level].length){
            // this.ngToast.create({className: 'info', content: 'Nada para ordenar...'});
            return;
        }

        this.uibModal.open({
            templateUrl: 'views/product/categories/order/order.html',
            controller: 'cProductCategoriesOrder',
            controllerAs: '$ctrl',
            resolve: {
                data: () => {
                    return {
                        levels: copy(this.levels.data[level]),
                        currentLevel: level,
                    }
                },
            },
        });
    }

    previousLink(currentLevel){
        let previousLevel = this.findLevel(currentLevel, 'previous');
        if(this.levels.selected.hasOwnProperty(previousLevel)) {
            const levelId = this.levels.selected[previousLevel];
            if(levelId){
                let previousLink = searchArray(this.levels.data[previousLevel], 'id', levelId).name;
                return this.filter('removeAccents')(previousLink.replace(/ /gi, '-').toLowerCase());
            }
        }
        return false;
    }
}