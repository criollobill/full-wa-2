const gulp   = require('gulp');
const clean  = require('gulp-clean');
const rename = require('gulp-rename');
const concat = require('gulp-concat');

const [FROM, DIST] = ['app', 'dist'];

const css = {
	compile: require('gulp-sass'),
	prefixer: require('gulp-autoprefixer'),
};

const js = {
	compile: require('gulp-babel'),
	hint: require('gulp-jshint'),
	minify: require('gulp-uglify'),
};

const html = {
	minify: require('gulp-html-minifier'),
};

const CONFIGS = {
	sass: { outputStyle: 'compressed' },
	prefixer: { browsers: ['> 3%'] },
	html: {
		caseSensitive: false,
		collapseBooleanAttributes: true,
		collapseInlineTagWhitespace: false,
		collapseWhitespace: true,
		conservativeCollapse: false,
		html5: true,
		keepClosingSlash: false,
		maxLineLength: false,
		minifyCSS: true,
		minifyJS: true,
		minifyURLs: false,
		preserveLineBreaks: false,
		preventAttributesEscaping: false,
		processConditionalComments: true,
		quoteCharacter: false,
		removeAttributeQuotes: true,
		removeComments: true,
		removeEmptyAttributes: true,
		removeEmptyElements: false,
		removeOptionalTags: true,
		removeRedundantAttributes: true,
		processScripts: ['text/html']
	},
	babel: { 
		presets: ['es2015'],
		comments : false,
		minified : false,
		compact  : true
	},
};

//////////////////////////////////////////
// Error Function
//////////////////////////////////////////
function errFn(err){
	console.log(err.toString());
	this.emit('end');
}

function cleanDist(){
	return gulp.src(DIST + '/', {read: false, force: true, allowEmpty: true})
	.pipe(clean())
		.on('error', errFn)
}

//////////////////////////////////////////
// Client
//////////////////////////////////////////
function ClientSass(){
	return gulp.src(FROM + '/sass/compiler.scss')
	.pipe(css.compile(CONFIGS.sass)
		.on('error', css.compile.logError))
	.pipe(css.prefixer(CONFIGS.prefixer))
		.on('error', errFn)
	.pipe(rename('css.css'))
	.pipe(gulp.dest(DIST))
		.on('error', errFn)
}

function ClientJs(){
	return gulp.src([
        FROM + '/js/functions.js',
		FROM + '/js/{constants,filters,directives,services,controllers}/*.js',
		FROM + '/views/**/*.js',
		FROM + '/components/**/*.js',
		FROM + '/js/main.js']
	).pipe(concat('js.js'))
		.on('error', errFn)
	.pipe(js.compile(CONFIGS.babel))
		.on('error', errFn)
	.pipe(gulp.dest(DIST))
		.on('error', errFn)
}

function ClientRoot(){
	return gulp.src([FROM + '/*.*', FROM + '/.htaccess'])
	.pipe(gulp.dest(DIST))
		.on('error', errFn)
}

function ClientRender(){
	return gulp.src(FROM + '/render/**/*.*')
	.pipe(gulp.dest(DIST + '/render'))
		.on('error', errFn)
}

function ClientImages(){
	return gulp.src(FROM + '/images/**/*.*')
	.pipe(gulp.dest(DIST + '/images'))
		.on('error', errFn)
}

function ClientPhp(){
	return gulp.src(FROM + '/php/**/*.*')
	.pipe(gulp.dest(DIST + '/php'))
		.on('error', errFn)
}

function ClientApi(){
	return gulp.src([FROM + '/api/**/*.*', FROM + '/api/.htaccess'])
	.pipe(gulp.dest(DIST + '/api'))
		.on('error', errFn)
}

function ClientFonts(){
	return gulp.src(FROM + '/fonts/**/*.*')
	.pipe(gulp.dest(DIST + '/fonts'))
		.on('error', errFn)
}

function ClientViews(){
	return gulp.src(FROM + '/views/**/*.{html,php}')
	.pipe(html.minify( CONFIGS.html))
		.on('error', errFn)
	.pipe(gulp.dest(DIST + '/views'))
		.on('error', errFn)
}

function ClientComponents(){
	return gulp.src(FROM + '/components/**/*.{html,php}')
	.pipe(html.minify( CONFIGS.html))
		.on('error', errFn)
	.pipe(gulp.dest(DIST + '/components'))
		.on('error', errFn)
}

//////////////////////////////////////////
// Adm
//////////////////////////////////////////
function AdmSass(){
	return gulp.src(FROM + '/adm/sass/compiler.scss')
	.pipe(css.compile(CONFIGS.sass)
		.on('error', css.compile.logError))
	.pipe(css.prefixer(CONFIGS.prefixer))
		.on('error', errFn)
	.pipe(rename('css.css'))
	.pipe(gulp.dest(DIST + '/adm'))
		.on('error', errFn)
}

function AdmJs(){
	return gulp.src([
        FROM + '/js/functions.js',
		FROM + '/js/{constants,directives,filters,services}/*.js',
		FROM + '/components/shared/**/*.js',
		FROM + '/adm/js/controllers/*.js',
		FROM + '/adm/views/**/*.js',
		FROM + '/adm/components/**/*.js',
		FROM + '/adm/js/main.js']
	).pipe(concat('js.js'))
		.on('error', errFn)
	.pipe(js.compile(CONFIGS.babel))
		.on('error', errFn)
	.pipe(gulp.dest(DIST + '/adm'))
		.on('error', errFn)
}

function AdmRoot(){
	return gulp.src([
		FROM + '/adm/*.*',
		FROM + '/adm/.htaccess']
	).pipe(gulp.dest(DIST + '/adm'))
		.on('error', errFn)
}

function AdmImages(){
	return gulp.src(FROM + '/adm/images/**/*.*')
	.pipe(gulp.dest(DIST + '/adm/images'))
		.on('error', errFn)
}

function AdmFonts(){
	return gulp.src(FROM + '/adm/fonts/**/*.*')
	.pipe(gulp.dest(DIST + '/adm/fonts'))
		.on('error', errFn)
}

function AdmViews(){
	return gulp.src(FROM + '/adm/views/**/*.html')
	.pipe(html.minify(CONFIGS.html))
		.on('error', errFn)
	.pipe(gulp.dest(DIST + '/adm/views'))
		.on('error', errFn)
}

function AdmComponents(){
	return gulp.src(FROM + '/adm/components/**/*.html')
	.pipe(html.minify( CONFIGS.html))
		.on('error', errFn)
	.pipe(gulp.dest(DIST + '/adm/components'))
		.on('error', errFn)
}

function AdmSharedComponents(){
	return gulp.src(FROM + '/components/shared/**/*.html')
	.pipe(html.minify( CONFIGS.html))
		.on('error', errFn)
	.pipe(gulp.dest(DIST + '/adm/components/shared'))
		.on('error', errFn)
}

//////////////////////////////////////////
// Global
//////////////////////////////////////////
function GlobalLibraries(){
	return gulp.src(FROM + '/libraries/**/*.js')
	.pipe(concat('libs.js'))
		.on('error', errFn)
	.pipe(js.minify())
		.on('error', errFn)
	.pipe(gulp.dest(DIST))
		.on('error', errFn)
}

//////////////////////////////////////////
// Watch STATICS
//////////////////////////////////////////
function auxRegex(path){
	const REGEX  = /^app\\(.*)/i;
	let complete = path.match(REGEX)[1];
	let split    = complete.split('\\');
	let file     = split.pop();
	let partial  = split.join('/');

	return [complete, partial, file];
}

function Watchers(){
	//////////////////////////////////////////
	// Watch Statics
	//////////////////////////////////////////
	const watchStaticsFolders = [
		FROM + '/*.*',
		FROM + '/php/**/*.php',
		FROM + '/api/**/*.php',
		FROM + '/images/**/*.{png,jpg,jpeg,gif,svg}',
		FROM + '/fonts/**/*.{otf,ttf,woff,woff2}',
		FROM + '/adm/*.*',
		FROM + '/adm/images/**/*.{png,jpg,jpeg,gif,svg}',
		FROM + '/adm/fonts/**/*.{otf,ttf,woff,woff2}',
		FROM + '/views/**/*.{html,php}',
		FROM + '/components/**/*.html',
		FROM + '/adm/views/**/*.html',
		FROM + '/adm/components/**/*.html'
	];

	let watchStatics = gulp.watch(watchStaticsFolders);
	watchStatics.on('change', function(event){
		const [complete, partial, file] = auxRegex(event);

		// JS MODIFIED IN ROOT
		if( file.match(/\.js$/i) ){
			console.log('______________________________________');
			console.log('Arquivo a ser analisado: ', file);

			gulp.src(FROM + '/' + complete)
	        .pipe(js.compile(CONFIGS.babel))
	        	.on('error', errFn)
	        .pipe(gulp.dest(DIST))
	        	.on('error', errFn)

	    // HTML
		}else if(file.match(/\.html$/i)){
			console.log('______________________________________');
			console.log('Arquivo copiado: ', file);

			gulp.src(FROM + '/' + complete)
			.pipe(html.minify(CONFIGS.html))
				.on('error', errFn)
			.pipe(gulp.dest(DIST))
				.on('error', errFn)

		// ALL OTHERS
		}else{
			console.log('______________________________________');
			console.log('Arquivo copiado: ', file);

			gulp.src(FROM + '/' + complete)
				.on('error', errFn)
			.pipe(gulp.dest(DIST))
				.on('error', errFn)
		}
	});

	watchStatics.on('add', function(event){
		const [complete, partial, file] = auxRegex(event);
		console.log('______________________________________');
		console.log('Arquivo adicionado: ', file);

		// JS ADDED IN ROOT
		if( file.match(/\.js$/i) ){
			console.log('______________________________________');
			console.log('Arquivo a ser analisado: ', file);

			gulp.src(FROM + '/' + complete)
	        .pipe(js.compile(CONFIGS.babel))
	        	.on('error', errFn)
	        .pipe(gulp.dest(DIST))
	        	.on('error', errFn)

	    // HTML
		}else if(file.match(/\.html$/i)){
			console.log('______________________________________');
			console.log('Arquivo copiado: ', file);

			gulp.src(FROM + '/' + complete)
			.pipe(html.minify(CONFIGS.html))
				.on('error', errFn)
			.pipe(gulp.dest(DIST))
				.on('error', errFn)

		// ALL OTHERS
		}else{
			console.log('______________________________________');
			console.log('Arquivo copiado: ', file);

			gulp.src(FROM + '/' + complete)
				.on('error', errFn)
			.pipe(gulp.dest(DIST))
				.on('error', errFn)
		}
	});

	//////////////////////////////////////////
	// Watch Libraries
	//////////////////////////////////////////
	gulp.watch(FROM + '/libraries/**/*.js', gulp.series(GlobalLibraries));

	//////////////////////////////////////////
	// Watch ClientJS
	//////////////////////////////////////////
	const watchClientJsFolders = [
		FROM + '/js/**/*.js',
		FROM + '/views/**/*.js',
		FROM + '/components/**/*.js'
	]

	const watchClientJs = gulp.watch(watchClientJsFolders, gulp.parallel(ClientJs));
	watchClientJs.on(['change'], function(event){
		const [complete, partial, file] = auxRegex(event);
		console.log('______________________________________');
		console.log('Arquivo a ser analisado: ', file);
		gulp.src(FROM + '/' + complete)
		.pipe(js.hint())
        	.on('error', errFn)
        .pipe(js.compile(CONFIGS.babel))
        	.on('error', errFn)
        .pipe(gulp.dest('analyse'))
        	.on('error', errFn)
	});

	//////////////////////////////////////////
	// Watch Adm JS
	//////////////////////////////////////////
	const watchAdmJsFolders = [
		FROM + '/js/{constants,directives,filters,services}/*.js',
		FROM + '/components/shared/**/*.js',
		FROM + '/adm/js/**/*.js',
		FROM + '/adm/views/**/*.js',
		FROM + '/adm/components/**/*.js'
	];

	const watchAdmJs = gulp.watch(watchAdmJsFolders, gulp.parallel(AdmJs));
	watchAdmJs.on(['change'], function(event){
		const [complete, partial, file] = auxRegex(event);
		console.log('______________________________________');
		console.log('Arquivo a ser analisado: ', file);
		gulp.src(FROM + '/' + complete)
		.pipe(js.hint())
        	.on('error', errFn)
        .pipe(js.compile(CONFIGS.babel))
        	.on('error', errFn)
        .pipe(gulp.dest('analyse/adm'))
        	.on('error', errFn)
	});

	//////////////////////////////////////////
	// Watch CSS
	//////////////////////////////////////////
	gulp.watch([FROM + '/sass/**/*.scss', FROM + '/views/**/*.scss', FROM + '/components/**/*.scss'], gulp.series(ClientSass));
	gulp.watch([FROM + '/sass/**/*.scss', FROM + '/components/shared/**/*.scss', FROM + '/adm/sass/**/*.scss', FROM + '/adm/views/**/*.scss', FROM + '/adm/components/**/*.scss'], gulp.series(AdmSass));
}

gulp.task('default', gulp.series(
	cleanDist,
	gulp.parallel(ClientSass, ClientJs, ClientRoot, ClientRender, ClientImages, ClientPhp, ClientApi, ClientFonts, ClientViews, ClientComponents),
	gulp.parallel(AdmSass, AdmJs, AdmRoot, AdmImages, AdmFonts, AdmViews, AdmComponents, AdmSharedComponents),
	GlobalLibraries,
	Watchers
));
